import rclpy
from rclpy.node import Node

from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist

import cv2
from cv_bridge import CvBridge
import numpy as np



LAPLACIAN_THRESHOLD = 20

class LineDetector(Node):

    def __init__(self):
        super().__init__('line_detector')
        self.create_subscription(Image,'/camera/image_raw',self.image_handler,10)

        self.y_top = 400
        self.y_bottom = 425
        self.middle = (self.y_top+self.y_bottom) // 2

        self.cmd_vel_pub = self.create_publisher(Twist, '/cmd_vel', 10)


    def line_detection(self):
        line_edges = []
        for y in range(self.y_top, self.y_bottom):
            lap = abs(self.second_derivitive(self.gray[y, :]))
            for x, val in enumerate(lap):
                if val >= LAPLACIAN_THRESHOLD:
                    line_edges.append([x,y])
                    # cv2.circle(self.cv_image, (x, y), 1, (0,255,0), -1)
            
        line_edges = np.array(line_edges)
        if len(line_edges) > 0:
            center = tuple(sum(line_edges) // len(line_edges))
            # cv2.circle(self.cv_image, center, 5, (255,255,0), -1)
            return center
        else:
            center = (self.img_width//2, self.middle)
            # cv2.circle(self.cv_image, center, 5, (0,0,0), -1)
            return None
        


    def second_derivitive(self, arr):
        return np.array([arr[i] -2*arr[i+1] + arr[i+2] for i in range(len(arr) - 2)])
            

    def image_handler(self, msg : Image):
        self.img_width = msg.width
        self.img_height = msg.height

        bridge = CvBridge()
        self.cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        self.gray = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)

        center = self.line_detection()
        self.drive_to(center, self.img_width)
        
        # cv2.line(self.cv_image, (0, self.y_top), (self.img_width, self.y_top), (255,0,0), 2)
        # cv2.line(self.cv_image, (0, self.y_bottom), (self.img_width, self.y_bottom), (255,0,0), 2)

        # cv2.imshow("camera video", self.cv_image)
        # cv2.waitKey(1)
    

    def drive_to(self, center, width):
        twist_msg = Twist()
        if center is None:
            twist_msg.linear.x = 0.0
            twist_msg.angular.z = 0.3
        else:
            center_direction = (width//2 - center[0]) / width
            twist_msg.angular.z = center_direction
            twist_msg.linear.x = 0.3 * (0.5 - abs(center_direction))
        
        self.cmd_vel_pub.publish(twist_msg)





def main(args=None):
    rclpy.init(args=args)
    line_detector = LineDetector()
    rclpy.spin(line_detector)
    line_detector.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
    