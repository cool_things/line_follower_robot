import os
from setuptools import setup
from glob import glob

package_name = 'my_bot_py'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'),
        glob(os.path.join('launch', '*launch.*'))),
        (os.path.join('share', package_name, 'description'),
        glob(os.path.join('description', '*')))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='bar',
    maintainer_email='developer8g@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "line_detector = my_bot_py.line_detector:main"
        ],
    },
)
